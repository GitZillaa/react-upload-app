const express = require('express');
const multer = require('multer');
const cors = require('cors');
const fs = require('fs');
const path = require('path');

const app = express();
const PORT = 5000;

// Konfiguriere Multer (Middleware für Dateiuploads)
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

const upload = multer({ storage: storage });

app.use(cors());
app.use(express.static('uploads'));

app.post('/upload', upload.single('file'), (req, res) => {
    res.status(201).send({ file: req.file });
});

app.get('/files', (req, res) => {
    fs.readdir('uploads', (err, files) => {
        if (err) {
            return res.status(500).send({ error: err });
        }
        res.status(200).send({ files });
    });
});

app.listen(PORT, () => {
    console.log(`Server lüpt auf http://localhost:${PORT}`);
});
