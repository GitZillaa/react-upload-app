import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [file, setFile] = useState(null);
  const [filesList, setFilesList] = useState([]);

  useEffect(() => {
    fetchFiles();
  }, []);

  const fetchFiles = async () => {
    const response = await axios.get('http://localhost:5000/files');
    setFilesList(response.data.files);
  };

  const onFileChange = event => {
    setFile(event.target.files[0]);
  };

  const onFileUpload = async () => {
    const formData = new FormData();
    formData.append('file', file);

    await axios.post('http://localhost:5000/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });

    fetchFiles();
  };

  return (
    <div>
      <h2>XXX</h2>
      <input type="file" onChange={onFileChange} />
      <button onClick={onFileUpload}>Hochladen!</button>
      <h3>Sammlung</h3>
      <ul>
        {filesList.map(file => (
          <li key={file}>{file}</li>
        ))}
      </ul>
    </div>
  );
}

export default App;
