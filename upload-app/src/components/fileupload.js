import axios from 'axios';
import React from 'react ';

const Fileupload = () => {
  const handelfileUpload = async (e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append('file', file);

    try {
      const res = await axios.post('http://localost:5000/upload', formData);
      console.log('Server-Antwort:', res.data);
    } catch (err) {
      console.error('Fehler beim Hochladen:', err.message);
    }
  }
  

  return (

    <div className="container">
      <h3>Hochgeladen</h3>
      <input type="file" onchange={handelfileUpload} />
    </div>
  );
};

export default Fileupload;
